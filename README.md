# SBP Address Book

Address Book is an online application built to manage customer Address with respect to Logistics Label Print.

It is and private project, and others doesn't have authority to modify or sale it without permission.

# Installation

* Install Apache Webserver
* Install PHP > 7.3 
* Install composer
* Install MySQL
* Create a Database & User

# Clone
* Clone Repository
* cd /clone/path
* ```composer i```
* ```cp .env.example .env```
* ```php artisan key:generate```
* ```php artisan migrate --seed```
